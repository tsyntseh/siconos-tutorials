# --- set required siconos version to run examples ---
set(MAJOR_VERSION 4)
set(MINOR_VERSION 2)
set(PATCH_VERSION 0)
set(SICONOS_REQUIRED_VERSION "${MAJOR_VERSION}.${MINOR_VERSION}.${PATCH_VERSION}")

